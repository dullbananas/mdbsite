import yaml
#from yaml import load, dump
#try:
	#from yaml import CLoader as Loader, CDumper as Dumper
#except ImportError:
	#from yaml import Loader, Dumper
import os


package_path = os.path.dirname(os.path.abspath(__file__))
menu_path = os.path.join(package_path, 'data/menu.yml')
with open(menu_path, 'r') as f:
	yaml_text = f.read()

menu_data = yaml.load(yaml_text)
