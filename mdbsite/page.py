import flask
from .menu import menu_data


bp = flask.Blueprint('page', __name__, url_prefix='/page')


@bp.route('/home')
def home():
	return flask.render_template('home.html')


@bp.route('/menu')
def menu():
	return flask.render_template('menu.html', menu_data=menu_data)


@bp.route('/about')
def about():
	return flask.render_template('about.html')
